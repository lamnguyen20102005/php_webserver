#install python
sudo dnf install python3
sudo dnf install python3-pip
pip3 install flask

#open port 8080
sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports

