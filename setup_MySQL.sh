#Login
sudo mysql

# Set up password
ALTER USER 'root'@'localhost' IDENTIFIED BY 'your_new_password';

# If you don't have a root user, you can create one and set a password: (Not neccessary)
CREATE USER 'root'@'localhost' IDENTIFIED BY 'your_new_password';

# Flush privileges to apply the changes:
FLUSH PRIVILEGES;

# Exit
exit;


# Log in to MySQL
mysql -u root -p

# Create a Database
CREATE DATABASE my_user;

# Create a Data Table
USE my_user;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    age INT NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone VARCHAR(20) NOT NULL,
    city VARCHAR(255) NOT NULL,
    state VARCHAR(255) NOT NULL
);

# Commit Changes:
COMMIT;



# Update Flask Application (remember)
db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="root",
    database="my_user"
)



# How to see your data from database:
USE my_user;
SELECT * FROM users;
exit;

#To show the DDL (Data Definition Language) for a MySQL table, you can use the SHOW CREATE TABLE command. Here's an example:
SHOW CREATE TABLE users;
