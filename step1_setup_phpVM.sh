#!/bin/bash

# Update system packages
sudo yum update -y

# Install EPEL repository
sudo yum install epel-release -y

# Install Apache web server
sudo yum install httpd -y

# Start and enable Apache
sudo systemctl start httpd
sudo systemctl enable httpd

# Install PHP and required modules
sudo yum install php php-mysqlnd php-dom php-gd php-mbstring -y

# Restart Apache to apply changes
sudo systemctl restart httpd

# Enable PHP to run on Apache
sudo echo "<?php phpinfo(); ?>" > /var/www/html/info.php

# Open firewall for HTTP traffic
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload

#Install vim
sudo yum install vim -y

