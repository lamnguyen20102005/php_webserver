from flask import Flask, render_template, request, redirect, url_for
import os
import mysql.connector

app = Flask(__name__, template_folder=os.path.abspath(os.path.dirname(__file__)))
db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="root",
    database="my_user"
)
cursor = db.cursor()

# Routes
@app.route('/')
def index():
    try:
        cursor.execute("SELECT * FROM users")
        users = cursor.fetchall()
        user_list = [{'id': user[0], 'username': user[1], 'age': user[2], 'email': user[3],
                      'phone': user[4], 'city': user[5], 'state': user[6]} for user in users]
        return render_template('index.html', users=user_list)
    except mysql.connector.Error as err:
        print(f"Error: {err}")
        return "An error occurred while fetching data from the database."

@app.route('/add', methods=['POST'])
def add():
    try:
        username = request.form['username']
        age = int(request.form['age'])
        email = request.form['email']
        phone = request.form['phone']
        city = request.form['city']
        state = request.form['state']

        cursor.execute("INSERT INTO users (username, age, email, phone, city, state) VALUES (%s, %s, %s, %s, %s, %s)",
                       (username, age, email, phone, city, state))
        db.commit()
        return redirect(url_for('index'))
    except (mysql.connector.Error, ValueError) as err:
        print(f"Error: {err}")
        return "An error occurred while adding a new user."

@app.route('/delete/<int:user_id>')
def delete(user_id):
    try:
        cursor.execute("DELETE FROM users WHERE id = %s", (user_id,))
        db.commit()
        return redirect(url_for('index'))
    except mysql.connector.Error as err:
        print(f"Error: {err}")
        return "An error occurred while deleting the user."

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
