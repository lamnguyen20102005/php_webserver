#!/bin/bash

# Install MySQL repository package
sudo dnf install https://dev.mysql.com/get/mysql80-community-release-el8-1.noarch.rpm

# Install MySQL server
sudo dnf install mysql-server

# Start MySQL service
sudo systemctl start mysqld

# Enable MySQL to start on boot
sudo systemctl enable mysqld

# Check the status of MySQL service
sudo systemctl status mysqld

# instal mysql-connector-python
pip3 install mysql-connector-python
pip3 install --user mysql-connector-python==8.0.22
